#include "parse.h"

#include <stdlib.h>
#include <string.h>

int
parse_packet (FILE *file, void **p_data, int *p_size)
{
	unsigned char *packet;
	unsigned char header[13];
	int n;
	int size;

	n = fread (header, 1, 13, file);
	if (feof(file)) {
		*p_data = NULL;
		*p_size = 0;
		return PARSE_EOF;
	}
	if (n < 13) {
		printf("\nerror: truncated header\n");
		return PARSE_ERROR;
	}

	if (header[0] != 'B' || header[1] != 'B' || header[2] != 'C' ||
			header[3] != 'D') {
		printf("\nerror: header magic incorrect\n");
		return PARSE_ERROR;
	}

	size = (header[5]<<24) | (header[6]<<16) | (header[7]<<8) | (header[8]);
	if (size == 0) {
		size = 13;
	}
	if (size < 13) {
		return PARSE_ERROR;
	}
	if (size > 16*1024*1024) {
		printf("\nerror: packet too large? (%d > 16777216)\n", size);
		return PARSE_ERROR;
	}

	packet = (unsigned char *)malloc (size);
	memcpy (packet, header, 13);
	n = fread (packet + 13, 1, size - 13, file);
	if (n < size - 13) {
		free (packet);
		return PARSE_ERROR;
	}

	*p_data = packet;
	*p_size = size;
	return PARSE_OK;
}

