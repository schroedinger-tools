#include <iostream>
#include <stdexcept>
#include <sstream>

#include <schroedinger/schrobitstream.h>
#include <schroedinger/schrovideoformat.h>

std::istream& operator>> (std::istream& in, SchroTransferFunction& out)
{
	std::string tf;
	in >> tf;

	if(tf == "0" || tf == "tv_gamma" ) { out = SCHRO_TRANSFER_CHAR_TV_GAMMA; }
	else if(tf == "1" || tf == "ext_gamut" ) { out = SCHRO_TRANSFER_CHAR_EXTENDED_GAMUT; }
	else if(tf == "2" || tf == "linear" ) { out = SCHRO_TRANSFER_CHAR_LINEAR; }
	else if(tf == "3" || tf == "dci_gamma" ) { out = SCHRO_TRANSFER_CHAR_DCI_GAMMA; }
	else {
		std::string err;
		err = "Transfer function '" + tf + "' is not recognised";
		throw std::runtime_error(err);
	}

	return in;
}

std::istream& operator>> (std::istream& in, SchroColourMatrix& out)
{
	std::string cp;
	in >> cp;

	if(cp == "0" || cp == "hdtv" ) { out = SCHRO_COLOUR_MATRIX_HDTV; }
	else if(cp == "1" || cp == "sdtv" ) { out = SCHRO_COLOUR_MATRIX_SDTV; }
	else if(cp == "2" || cp == "reversible" ) { out = SCHRO_COLOUR_MATRIX_REVERSIBLE; }
	else {
		std::string err;
		err = "Colour matrix '" + cp + "' is not recognised";
		throw std::runtime_error(err);
	}

	return in;
}

std::istream& operator>> (std::istream& in, SchroColourPrimaries& out)
{
	std::string cp;
	in >> cp;

	if(cp == "0" || cp == "hdtv" ) { out = SCHRO_COLOUR_PRIMARY_HDTV; }
	else if(cp == "1" || cp == "sdtv_525" ) { out = SCHRO_COLOUR_PRIMARY_SDTV_525; }
	else if(cp == "2" || cp == "sdtv_625" ) { out = SCHRO_COLOUR_PRIMARY_SDTV_625; }
	else if(cp == "2" || cp == "cinema" ) { out = SCHRO_COLOUR_PRIMARY_CINEMA; }
	else {
		std::string err;
		err = "Colour primary '" + cp + "' is not recognised";
		throw std::runtime_error(err);
	}

	return in;
}

std::istream& operator>> (std::istream& in, SchroVideoFormatEnum& out)
{
	std::string preset;
	in >> preset;

	if(preset == "QSIF") { out = SCHRO_VIDEO_FORMAT_QSIF; }
	else if(preset == "QCIF") { out = SCHRO_VIDEO_FORMAT_QCIF; }
	else if(preset == "SIF") { out = SCHRO_VIDEO_FORMAT_SIF; }
	else if(preset == "CIF") { out = SCHRO_VIDEO_FORMAT_CIF; }
	else if(preset == "4SIF") { out = SCHRO_VIDEO_FORMAT_4SIF; }
	else if(preset == "4CIF") { out = SCHRO_VIDEO_FORMAT_4CIF; }
	else if(preset == "SD480I_60") { out = SCHRO_VIDEO_FORMAT_SD480I_60; }
	else if(preset == "SD576I_50") { out = SCHRO_VIDEO_FORMAT_SD576I_50; }
	else if(preset == "HD720P_60") { out = SCHRO_VIDEO_FORMAT_HD720P_60; }
	else if(preset == "HD720P_50") { out = SCHRO_VIDEO_FORMAT_HD720P_50; }
	else if(preset == "HD1080I_60") { out = SCHRO_VIDEO_FORMAT_HD1080I_60; }
	else if(preset == "HD1080I_50") { out = SCHRO_VIDEO_FORMAT_HD1080I_50; }
	else if(preset == "HD1080P_60") { out = SCHRO_VIDEO_FORMAT_HD1080P_60; }
	else if(preset == "HD1080P_50") { out = SCHRO_VIDEO_FORMAT_HD1080P_50; }
	else if(preset == "DC2K_24") { out =  SCHRO_VIDEO_FORMAT_DC2K_24; }
	else if(preset == "DC4K_24") { out = SCHRO_VIDEO_FORMAT_DC4K_24; }
	else {
		std::string err;
		err = "Video format '" + preset + "' is not recognised";
		throw std::runtime_error(err);
	}

	return in;
}

std::istream& operator>> (std::istream& in, SchroChromaFormat& out)
{
	std::string cf;
	in >> cf;

	if(cf == "0" || cf == "444" ) { out = SCHRO_CHROMA_444; }
	else if(cf == "1" || cf == "422" ) { out = SCHRO_CHROMA_422; }
	else if(cf == "2" || cf == "420" ) { out = SCHRO_CHROMA_420; }
	else {
		std::string err;
		err = "Chroma format '" + cf + "' is not recognised";
		throw std::runtime_error(err);
	}

	return in;
}


