/* Schrodinger decoder wrapper to for non GStreamer applications
 * derived from gstschrodec.c & testsuite/decode.c
 * 2006 Jon Rosser BBC R&D
 *
 */

#undef _FORTIFY_SOURCE
#define _FORTIFY_SOURCE 0

#include <iostream>
#include <string>

#include <stdio.h>
#include <cstdio>
#include <string.h>
#include <errno.h>

#include <schroedinger/schro.h>
#include <schroedinger/schroparams.h>

#include <boost/program_options.hpp>
#include "df_boost_prog_opts.hpp"
namespace po = boost::program_options;

#include "common.h"
#include "parse.h"

/* global variables */
static std::string inFileName;
static std::string outFileName;
static bool verbose = false;
static bool split = false;

/* function prototypes */
static bool parseCommandLine(int argc, char *argv[]);
static void decode();

int main(int argc, char **argv)
{

	schro_init();

	if(parseCommandLine(argc, argv)) {
		exit(1);
	}

	decode();

	return 0;
}

bool parseCommandLine(int argc, char **argv)
{
	bool ret=false;

	try {

		//Video format options
		po::options_description desc("Allowed Options");
		desc.add_options()
		("help",      fixes::po::value<bool>(),         "        Show this message")
		("verbose,v", fixes::po::bool_switch(&verbose), "        Enable verbose decoding")
		("split,s",   fixes::po::bool_switch(&split), "        Split output into frames [put %d in filename]")
		("input",     fixes::po::value(&inFileName),    "string  Dirac bitstream to decode")
		("output",    fixes::po::value(&outFileName),   "string  YUV planar output file");

		//the filenames are 'positional options'
		po::positional_options_description p;
		p.add("input", 1);
		p.add("output", 2);

		//do the command line parsing
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv). options(desc).positional(p).run(), vm);
		po::notify(vm);

		//decode flags
		if(vm.count("help")) {
			std::cerr << "Usage : schro_decode [flags] input output" << std::endl;
			std::cerr << desc << std::endl;
			ret = true;
		}

		if(vm.count("input") == 0)
			throw(std::runtime_error("No input file specified"));

		if(vm.count("output") == 0)
			throw(std::runtime_error("No output file specified"));

	}
	catch(std::exception& e)
	{
		fprintf(stderr, "Command line error : ");
		std::cerr << e.what() << "\n";
		ret = true;
	}

	return ret;
}

static void
buffer_free (SchroBuffer *buf, void *priv)
{
	free (priv);
}

static void write_component(FILE *outfile, SchroFrameData *comp)
{
	if (comp->width == comp->stride)
		fwrite(comp->data, comp->length, 1, outfile);
	else
	{
		unsigned char *line = (unsigned char *)comp->data;
		for (int j = 0; j < comp->height; ++j)
		{
			fwrite(line, comp->width, 1, outfile);
			line += comp->stride;
		}	
	}
	return;
}

/* push the data through the decoder */
static void decode()
{
	FILE *infile;
	FILE *outfile = NULL;

	SchroDecoder *decoder;
	SchroBuffer *buffer;
	SchroVideoFormat *format = NULL;
	SchroFrame *frame;
	bool go;
	void *packet;
	int size=-1;
	int it;
	int ret;
	int framecount=0;
	char formatted_name[1024];

	/* open an input and output files */
	if (strcmp(inFileName.c_str(), "-") == 0)
	{
		inFileName = std::string("/dev/stdin");
	}
	if (strcmp(outFileName.c_str(), "-") == 0)
	{
		if (split == false)
			outFileName = std::string("/dev/stdout");
		else
		{
			fprintf(stderr, "Cannot split output into separate files when writing to stdout\n");
			exit(1);
		}
	}
	infile  = fopen(inFileName.c_str(), "rb");
	if(infile == NULL) {
		fprintf(stderr, "Could not open input file %s\n", inFileName.c_str());
		exit(1);
	}

	decoder = schro_decoder_new();

	while(true) {

		//on the previous iteration feof(inFile) = true, so stop
		if(size == 0)
			goto out;

		//get a packet from the stream and push it to the decoder
		ret = parse_packet (infile, &packet, &size);

		if(ret == PARSE_ERROR)
			goto out;

		if (size == 0) {
			//unexpected end of file from parser
			schro_decoder_push_end_of_stream (decoder);
		} else {
			buffer = schro_buffer_new_with_data (packet, size);
			buffer->free = buffer_free;
			buffer->priv = packet;

			it = schro_decoder_push (decoder, buffer);
			if (it == SCHRO_DECODER_FIRST_ACCESS_UNIT) {
				format = schro_decoder_get_video_format (decoder);
				if(verbose) {
					fprintf(stderr, "\nFirst Sequence Header:\n");
					showVideoFormat(format);
				}
			}
		}

		//iterate the decoder until it needs more data
		go = true;
		while (go == true) {
			it = schro_decoder_wait (decoder);

			switch (it) {
			case SCHRO_DECODER_NEED_BITS:
				go = false;
				break;
			case SCHRO_DECODER_NEED_FRAME:
				switch(format->chroma_format) {
				case SCHRO_CHROMA_444:
					frame = schro_frame_new_and_alloc (NULL, SCHRO_FRAME_FORMAT_U8_444,
							format->width, format->height);
					break;
				case SCHRO_CHROMA_422:
					frame = schro_frame_new_and_alloc (NULL, SCHRO_FRAME_FORMAT_U8_422,
							format->width, format->height);
					break;
				case SCHRO_CHROMA_420:
					frame = schro_frame_new_and_alloc (NULL, SCHRO_FRAME_FORMAT_U8_420,
							format->width, format->height);
					break;
				default:
					fprintf(stderr, "error: unrecognised chroma format\n");
					exit(1);
					break;
				}
				schro_decoder_add_output_picture (decoder, frame);
				break;
			case SCHRO_DECODER_OK:
				{
					uint32_t picnum = schro_decoder_get_picture_number (decoder);
					frame = schro_decoder_pull (decoder);

					if(!frame) {
						break;
					}

					if(split == true || outfile == NULL) {
						snprintf(formatted_name, 1024, outFileName.c_str(), framecount);

						outfile = fopen(formatted_name, "wb");
						if(outfile == NULL) {
							fprintf(stderr, "Could not open output file %s\n", formatted_name);
							exit(1);
						}
					}

					if(verbose) {
						fprintf(stderr, "writing output frame %d (picnum %u) to %s\r", framecount, picnum, formatted_name);
						fflush(stdout);
					}

					// write frame data out in planar format
					write_component(outfile, &(frame->components[0]));
					write_component(outfile, &(frame->components[1]));
					write_component(outfile, &(frame->components[2]));
					schro_frame_unref(frame);

	                                if(split) fclose(outfile);

					framecount++;
				}
				break;
			case SCHRO_DECODER_EOS:
				if(verbose) fprintf(stderr, "\nGot End of Sequence");
				schro_decoder_reset(decoder);
				go = false;
				break;
			case SCHRO_DECODER_ERROR:
				fprintf(stderr, "\nerror: got decoder internal error - exiting\n");
				goto out;
				break;
			}
		}
	}

out:

	if(verbose) fprintf(stderr, "\nFreeing decoder");
	schro_decoder_free (decoder);
	free(format);

	fclose(infile);
	fclose(outfile);
}


