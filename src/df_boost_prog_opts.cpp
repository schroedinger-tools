
#include <boost/program_options.hpp>
#include "df_boost_prog_opts.hpp"

fixes::po::typed_value<bool>*
fixes::po::bool_switch(bool* v, bool default_v)
{
	fixes::po::typed_value<bool>* r = new fixes::po::typed_value<bool>(v);
#if BOOST_VERSION >= 103500
	r->implicit_value(default_v);
#endif
	r->default_value(!default_v);
	r->zero_tokens();
	return r;
}

fixes::po::typed_value<bool>*
fixes::po::bool_switch(bool default_v)
{
	return fixes::po::bool_switch(0, default_v);
}
