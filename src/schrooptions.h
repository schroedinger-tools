#ifndef __SCHROOPTIONS_H__
#define __SCHROOPTIONS_H__

#include <schroedinger/schro.h>
#include <boost/program_options.hpp>

void addDynamicOptions(boost::program_options::options_description &);
void addVideoCodingOptions(boost::program_options::options_description &);
void addVideoMetadataOptions(boost::program_options::options_description &);

bool applyDynamicOptions(SchroEncoder *, boost::program_options::variables_map &);
void applyVideoCodingOptions(SchroVideoFormat *, boost::program_options::variables_map &);
void applyVideoMetadataOptions(SchroVideoFormat *, boost::program_options::variables_map &);

#endif

