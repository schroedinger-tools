#ifndef __PARSE_H__
#define __PARSE_H__

#include <stdio.h>

enum {
	PARSE_EOF=-2,
	PARSE_ERROR=-1,
	PARSE_OK=0
};

int parse_packet(FILE *, void**, int *);

#endif

