/* Schrodinger recoder for non GStreamer applications
 * 2008 Jon Rosser BBC R&D
 *
 */

#undef _FORTIFY_SOURCE
#define _FORTIFY_SOURCE 0

#include <iostream>
#include <stdio.h>

#include <schroedinger/schro.h>

#include "schrooptions.h"
#include <boost/program_options.hpp>
#include "df_boost_prog_opts.hpp"
namespace po = boost::program_options;

#include "common.h"
#include "parse.h"

/* global variables */
static std::string inFileName;
static std::string outFileName;
static FILE *inFile;
static FILE *outFile;
static SchroVideoFormat *format = NULL;
static SchroDecoder *decoder = NULL;
static SchroEncoder *encoder = NULL;
static bool verbose = false;
static int encoder_frame=0, decoder_frame=-1;

/* function prototypes */
static bool parseCommandLine(int argc, char *argv[]);

static void buffer_free (SchroBuffer *buf, void *priv)
{
	free (priv);
}

//run the encoder until it needs another input picture, or produces an error
int runEncoder(SchroFrame *f)
{
	bool frame_waiting=true;
	SchroBuffer *buffer;
	int ret=0;

	while(true)
	{
		switch(schro_encoder_wait(encoder))
		{
			case SCHRO_STATE_NEED_FRAME:
				if(frame_waiting) {
					schro_encoder_push_frame(encoder, f);
					frame_waiting = false;
				}
				else
					goto out;
				break;

			case SCHRO_STATE_HAVE_BUFFER:
				buffer = schro_encoder_pull (encoder, &encoder_frame);
				fwrite(buffer->data, 1, buffer->length, outFile);
				schro_buffer_unref (buffer);
				break;

			case SCHRO_STATE_AGAIN:
				break;

			case SCHRO_STATE_END_OF_STREAM:
				// pull end of stream info
				buffer = schro_encoder_pull (encoder, &encoder_frame);
				if(verbose) printf("Encoder output end of sequence after frame %d\n", encoder_frame);
				buffer = schro_encoder_pull (encoder, &encoder_frame);
				fwrite(buffer->data, 1, buffer->length, outFile);
				schro_buffer_unref (buffer);
				ret=1;
				goto out;
				break;

			default:
			break;
		}
	}

out:
	return ret;
}


//run the decoder until it produces a picture, or an error 
int runDecoder(SchroFrame **f)
{
	void *packet=NULL;
	int size = 0;
	SchroBuffer *buffer;
	SchroFrame *inFrame;
	int it;
	int ret=PARSE_OK;

	//in case of error, we do not return a picture
	*f = NULL;

	//iterate the decoder until it produces a picture
	while (true) {
		it = schro_decoder_wait (decoder);

		switch (it) {

		case SCHRO_DECODER_NEED_BITS:
			//get a packet from the stream and push it to the decoder
			ret = parse_packet (inFile, &packet, &size);
			
			if(ret == PARSE_ERROR)
				return PARSE_ERROR;

			if (size == 0) {
				//unexpected end of file from parser
				schro_decoder_push_end_of_stream (decoder);
			} else {
				buffer = schro_buffer_new_with_data (packet, size);
				buffer->free = buffer_free;
				buffer->priv = packet;

				it = schro_decoder_push (decoder, buffer);
				if (it == SCHRO_DECODER_FIRST_ACCESS_UNIT) {
					format = schro_decoder_get_video_format (decoder);
					schro_encoder_set_video_format(encoder, format);
					schro_encoder_start(encoder);
					if(verbose) {
						printf("\nFirst Input Sequence Header, configuring encoder format:\n");
						showVideoFormat(format);
					}
				}
			}
			break;

		case SCHRO_DECODER_NEED_FRAME:		
			switch(format->chroma_format) {
			case SCHRO_CHROMA_444:
				inFrame = schro_frame_new_and_alloc (NULL, SCHRO_FRAME_FORMAT_U8_444,
						format->width, format->height);
				break;
			case SCHRO_CHROMA_422:
				inFrame = schro_frame_new_and_alloc (NULL, SCHRO_FRAME_FORMAT_U8_422,
						format->width, format->height);
				break;
			case SCHRO_CHROMA_420:
				inFrame = schro_frame_new_and_alloc (NULL, SCHRO_FRAME_FORMAT_U8_420,
						format->width, format->height);
				break;
			default:
				printf("error: unrecognised chroma format\n");
				exit(1);
				break;
			}
			schro_decoder_add_output_picture (decoder, inFrame);
			break;

		case SCHRO_DECODER_OK:
			*f = schro_decoder_pull (decoder);
			decoder_frame++;
			goto out;
			break;

		case SCHRO_DECODER_EOS:
			if(verbose) printf("\nDecoder Got End of Sequence\n");
			schro_decoder_reset(decoder);
			schro_encoder_end_of_stream(encoder);
			goto out;
			break;

		case SCHRO_DECODER_ERROR:
			printf("\nerror: got decoder internal error - exiting\n");
			return -1;
			break;
		}
	}

out:

	return ret;
}

/* main coding loop */
int main(int argc, char *argv[])
{
	SchroBuffer *buffer = NULL;
	SchroFrame *frame = NULL;
	int go = 1;

	schro_init();
	encoder = schro_encoder_new();
	decoder = schro_decoder_new();

	if(parseCommandLine(argc, argv)) {
		exit(1);
	}

	if(verbose)
		showEncoderSettings(encoder);

	/* open an input and output files */
	inFile = fopen(inFileName.c_str(), "rb");
	if (inFile == NULL)
	{
		printf("Could not open input file %s\n", inFileName.c_str());
		exit(1);
	}

	outFile = fopen(outFileName.c_str(), "wb");
	if (outFile == NULL)
	{
		printf("Could not open output file %s\n", outFileName.c_str());
		exit(1);
	}

	while(true) {
		
		//run the decoder until parse error
		if(runDecoder(&frame) == PARSE_ERROR)
			goto out;

		//run the encoder until end of sequence
		if(runEncoder(frame)) {
			goto out;
		}

		printf("Decoder frame %d, Encoder frame %d\r", decoder_frame, encoder_frame);
	}
out:	

	if(verbose) printf("\nFinished encoding\n");
	schro_encoder_free(encoder);
	schro_decoder_free(decoder);

	fclose(inFile);
	fclose(outFile);
	return 0;
}

bool parseCommandLine(int argc, char **argv)
{
	int wrap_length=800;	//make the internal line from boost have a really long line. The terminal does wrapping instead
	po::variables_map vm;

	try {

		po::options_description other_options("Other", wrap_length);
		other_options.add_options()
		("verbose,v", fixes::po::bool_switch(&verbose), "Show encoder settings before encoding")
		("input",     fixes::po::value(&inFileName),    "Dirac bitstream input file")
		("output",    fixes::po::value(&outFileName),   "Dirac bitstream output file")
		("help",      fixes::po::value<bool>(),         "Show help");

		//add dynamic options
		po::options_description dynamic_options("Encoder Coding Options", wrap_length);
		addDynamicOptions(dynamic_options);

		//merge the groups of option descriptions
		po::options_description all("Allowed Options");
		all.add(dynamic_options).add(other_options);

		//the filenames are 'positional options' 
		po::positional_options_description p;
		p.add("input", 1);
		p.add("output", 2);

		//do the command line parsing
		po::store(po::command_line_parser(argc, argv). options(all).positional(p).run(), vm);
		po::notify(vm);

		//decode some overall flags
		if(vm.count("help")) {
			std::cout << "Usage : schro_recode [flags] input output" << std::endl;
			std::cout << dynamic_options << std::endl;
			std::cout << other_options << std::endl;
			return true;
		}
	}
	catch(std::exception& e)
	{
		printf("Command line error : ");
		std::cout << e.what() << "\n";
		return true;
	}

	return applyDynamicOptions(encoder, vm);
}
