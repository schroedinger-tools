#include <stdio.h>
#include <string.h>
#include <iostream>
#include <string.h>

#include "common.h"
#include "schrooperators.h"

#define ROUND_UP_SHIFT(x,y) (((x) + (1<<(y)) - 1)>>(y))
#define ROUND_UP_POW2(x,y) (((x) + (1<<(y)) - 1)&((~0)<<(y)))

void showVideoFormat(SchroVideoFormat *f)
{
	fprintf(stderr, "Video Format:\n");
	fprintf(stderr, "  VideoFormatEnum         : %d\n", f->index);
	fprintf(stderr, "  Width                   : %d\n", f->width);
	fprintf(stderr, "  Height                  : %d\n", f->height);
	fprintf(stderr, "  Chroma format           : %d\n", (int)f->chroma_format);
	fprintf(stderr, "  Interlaced              : %d\n", f->interlaced);
	fprintf(stderr, "  Top field first         : %d\n", f->top_field_first);
	fprintf(stderr, "  Frame Rate Numerator    : %d\n", f->frame_rate_numerator);
	fprintf(stderr, "  Frame Rate Denominator  : %d\n", f->frame_rate_denominator);
	fprintf(stderr, "  Aspect Ratio Numerator  : %d\n", f->aspect_ratio_numerator);
	fprintf(stderr, "  Aspect Ratio Denominator: %d\n", f->aspect_ratio_denominator);
	fprintf(stderr, "  Clean width             : %d\n", f->clean_width);
	fprintf(stderr, "  Clean height            : %d\n", f->clean_height);
	fprintf(stderr, "  Left offset             : %d\n", f->left_offset);
	fprintf(stderr, "  Top offset              : %d\n", f->top_offset);
	fprintf(stderr, "  Luma offset             : %d\n", f->luma_offset);
	fprintf(stderr, "  Luma excursion          : %d\n", f->luma_excursion);
	fprintf(stderr, "  Chroma offset           : %d\n", f->chroma_offset);
	fprintf(stderr, "  Chroma excursion        : %d\n", f->chroma_excursion);
	fprintf(stderr, "  Colour primaries        : %d\n", f->colour_primaries);
	fprintf(stderr, "  Colour matrix           : %d\n", f->colour_matrix);
	fprintf(stderr, "  Transfer Function       : %d\n", f->transfer_function);
	fprintf(stderr, "  Interlaced Coding       : %d\n", f->interlaced_coding);
	fprintf(stderr, "\n");
}

int getMaxSettingNameLength()
{
	int maxlen = 0;
	int n = schro_encoder_get_n_settings();

	for(int i=0; i<n; i++) {
		const SchroEncoderSetting *s = schro_encoder_get_setting_info(i);
		int len = strlen(s->name);
		if(len > maxlen) maxlen = len;
	}

	return maxlen;
}

void showEncoderSettings(SchroEncoder *encoder)
{
	int n = schro_encoder_get_n_settings();
	int maxlen = getMaxSettingNameLength();

	fprintf(stderr, "Encoder Settings:\n");

	for(int i=0; i<n; i++) {
		const SchroEncoderSetting *s = schro_encoder_get_setting_info(i);
		double val = schro_encoder_setting_get_double(encoder, s->name);

		fprintf(stderr, "  %s%.*s", s->name, (int)(maxlen-strlen(s->name)+1), "                             ");

		switch(s->type) {
		case SCHRO_ENCODER_SETTING_TYPE_BOOLEAN:
			fprintf(stderr, "BOOL    %d\n", (int)val);
			break;

		case SCHRO_ENCODER_SETTING_TYPE_INT:
			fprintf(stderr, "INT     %d\n", (int)val);
			break;

		case SCHRO_ENCODER_SETTING_TYPE_DOUBLE:
			fprintf(stderr, "DOUBLE  %g\n", val);
			break;

		case SCHRO_ENCODER_SETTING_TYPE_ENUM:
			fprintf(stderr, "ENUM    %s\n", s->enum_list[(int)val]);
			break;

		default:
			break;
		}
	}

	fprintf(stderr, "\n");
}

SchroFrame *
frame_new_from_data_u8_planar422 (unsigned char *data, int width, int height)
{
  SchroFrame *frame = schro_frame_new();

  frame->format = SCHRO_FRAME_FORMAT_U8_422;

  frame->width = width;
  frame->height = height;

  frame->components[0].width = width;
  frame->components[0].height = height;
  frame->components[0].stride = ROUND_UP_POW2(width,2);
  frame->components[0].data = data;
  frame->components[0].length = frame->components[0].stride *
    ROUND_UP_POW2(frame->components[0].height,1);
  frame->components[0].v_shift = 0;
  frame->components[0].h_shift = 0;

  frame->components[1].width = ROUND_UP_SHIFT(width,1);
  frame->components[1].height = height;
  frame->components[1].stride = ROUND_UP_POW2(frame->components[1].width,2);
  frame->components[1].length =
    frame->components[1].stride * frame->components[1].height;
  frame->components[1].data = data + frame->components[0].length;
  frame->components[1].v_shift = 0;
  frame->components[1].h_shift = 1;

  frame->components[2].width = ROUND_UP_SHIFT(width,1);
  frame->components[2].height = height;
  frame->components[2].stride = ROUND_UP_POW2(frame->components[2].width,2);
  frame->components[2].length =
    frame->components[2].stride * frame->components[2].height;
  frame->components[2].data = data + frame->components[0].length + frame->components[1].length;
  frame->components[2].v_shift = 0;
  frame->components[2].h_shift = 1;

  return frame;
}

SchroFrame *
frame_new_from_data_u8_planar444 (unsigned char *data, int width, int height)
{
  SchroFrame *frame = schro_frame_new();

  frame->format = SCHRO_FRAME_FORMAT_U8_444;

  frame->width = width;
  frame->height = height;

  frame->components[0].width = width;
  frame->components[0].height = height;
  frame->components[0].stride = ROUND_UP_POW2(width,2);
  frame->components[0].data = data;
  frame->components[0].length = frame->components[0].stride *
    ROUND_UP_POW2(frame->components[0].height,1);
  frame->components[0].v_shift = 0;
  frame->components[0].h_shift = 0;

  frame->components[1].width = width;
  frame->components[1].height = height;
  frame->components[1].stride = ROUND_UP_POW2(frame->components[1].width,2);
  frame->components[1].length =
    frame->components[1].stride * frame->components[1].height;
  frame->components[1].data = data + frame->components[0].length;
  frame->components[1].v_shift = 0;
  frame->components[1].h_shift = 0;

  frame->components[2].width = width;
  frame->components[2].height = height;
  frame->components[2].stride = ROUND_UP_POW2(frame->components[2].width,2);
  frame->components[2].length =
    frame->components[2].stride * frame->components[2].height;
  frame->components[2].data = data + frame->components[0].length + frame->components[1].length;
  frame->components[2].v_shift = 0;
  frame->components[2].h_shift = 0;

  return frame;
}

