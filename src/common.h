#ifndef __COMMON_H__
#define __COMMON_H__

#include <schroedinger/schro.h>

void showEncoderSettings(SchroEncoder *encoder);
void showVideoFormat(SchroVideoFormat *f);
SchroFrame *frame_new_from_data_u8_planar422 (unsigned char *data, int width, int height);
SchroFrame *frame_new_from_data_u8_planar444 (unsigned char *data, int width, int height);

#endif

