#ifndef __SCHROOPERATORS_H__
#define __SCHROOPERATORS_H__

#include <iostream>

//stream to schro enumerations
std::istream& operator>> (std::istream& in, SchroTransferFunction& out);
std::istream& operator>> (std::istream& in, SchroColourMatrix& out);
std::istream& operator>> (std::istream& in, SchroColourPrimaries& out);
std::istream& operator>> (std::istream& in, SchroVideoFormatEnum& out);
std::istream& operator>> (std::istream& in, SchroChromaFormat& out);

#endif

