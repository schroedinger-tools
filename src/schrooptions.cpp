#include <boost/shared_ptr.hpp>
#include <iostream>
#include <string>
#include <math.h>

#include "df_boost_prog_opts.hpp"
#include "schrooptions.h"
#include "schrooperators.h"

namespace po = boost::program_options;

void framerateHelper(std::string fr, SchroVideoFormat *format)
{
	if(fr == "59.94")
	{
		format->frame_rate_numerator = 60000;
		format->frame_rate_denominator = 1001;
		return;
	}

	if(fr == "25")
	{
		format->frame_rate_numerator = 25;
		format->frame_rate_denominator = 1;
		return;
	}

	if(fr == "23.98")
	{
		format->frame_rate_numerator = 24000;
		format->frame_rate_denominator = 1001;
		return;
	}

	if(fr == "29.97")
	{
		format->frame_rate_numerator = 30000;
		format->frame_rate_denominator = 1001;
		return;
	}

	const char *cstr = fr.c_str();

	// test for decimal format
	if (strcspn(cstr, ".") != strlen(cstr))
	{
		// look for whole number
		char *num_token = strtok((char *)cstr, ".");
		int whole = strtoul(num_token, NULL, 10);
		int decimal = 0;
		int decimal_length = 0;

		// look for decimal part
		num_token = strtok(NULL, "");
		if (num_token)
		{
			decimal_length = strlen(num_token);
			decimal = strtoul(num_token, NULL, 10);
		}
		// calculate amount to raise to whole number
		int multiply = (int)pow(10.0, decimal_length);

		format->frame_rate_numerator = (decimal == 0) ? whole : (multiply * whole) + decimal;
		format->frame_rate_denominator = (decimal == 0) ? 1 : multiply;
	}
	else
	{
		// assume e/d format
		char *token = strtok((char *)cstr, "/");

		format->frame_rate_numerator = strtoul(token, NULL, 10);
		format->frame_rate_denominator = 1;

		token = strtok(NULL, "");
		if (token)
			format->frame_rate_denominator = strtoul(token, NULL, 10);
	}
}

void addVideoCodingOptions(po::options_description &vc_opts)
{
	vc_opts.add_options()
	("preset,p",          fixes::po::value<SchroVideoFormatEnum>(), "string  HD1080P_50\t Set compression preset "
	                                                         "QSIF QCIF SIF CIF 4SIF 4CIF "
	                                                         "SD480I_60 SD576I_50 HD720P_60 "
                                                                 "HD720P_50 HD1080I_50 HD1080I_60 "
                                                                 "HD1080P_50 HD1080P_60 "
		                                                 "DC2K_24 DC4K_24")
	("width,w",           fixes::po::value<unsigned int>(),         "uint    Preset\t  Width of video luminance")
	("height,h",          fixes::po::value<unsigned int>(),         "uint    Preset\t  Height of video luminance")
	("height,h",          fixes::po::value<unsigned int>(),         "uint    Preset\t  Height of video luminance")
	("cformat",           fixes::po::value<SchroChromaFormat>(),    "enum    Preset\t  Chroma subsampling [444|422|420]")
	("8bitvid",                                              "        Preset\t  Signal levels for 8 bit video")
	("8bitcg",                                               "        Preset\t  Signal levels for 8 bit graphics")
	("luma_offset",       fixes::po::value<unsigned int>(),         "uint    Preset\t  Luminance offset")
	("luma_excursion",    fixes::po::value<unsigned int>(),         "uint    Preset\t  Luminance offset")
	("chroma_offset",     fixes::po::value<unsigned int>(),         "uint    Preset\t  Chrominance offset")
	("chroma_excursion",  fixes::po::value<unsigned int>(),         "uint    Preset\t  Chrominance offset")
	("fr",                fixes::po::value<std::string>(),          "string  Preset\t  Frame rate, e.n or e/n format");
}

void applyVideoCodingOptions(SchroVideoFormat *format, po::variables_map &vm)
{
	//setup the video format
	if(vm.count("preset"))
		schro_video_format_set_std_video_format(format, vm["preset"].as<SchroVideoFormatEnum>());
	else
		schro_video_format_set_std_video_format(format, SCHRO_VIDEO_FORMAT_HD1080P_50);

	if(vm.count("width")) format->width = vm["width"].as<unsigned int>();
	if(vm.count("height")) format->height = vm["height"].as<unsigned int>();
	if(vm.count("cformat")) format->chroma_format = vm["cformat"].as<SchroChromaFormat>();
	if(vm.count("fr")) framerateHelper(vm["fr"].as<std::string>(), format);

	if(vm.count("8bitvid")) {
		format->luma_offset      = 16;
		format->luma_excursion   = 219;	
		format->chroma_offset    = 128;
		format->chroma_excursion = 224;
	}

	if(vm.count("8bitcg")) {
		format->luma_offset      = 0;
		format->luma_excursion   = 255;
		format->chroma_offset    = 128;
		format->chroma_excursion = 255;
	}

	if(vm.count("luma_offset")) format->luma_offset=vm["luma_offset"].as<int>();
	if(vm.count("luma_excursion")) format->luma_excursion=vm["luma_excursion"].as<int>();
	if(vm.count("chroma_offset")) format->chroma_offset=vm["chroma_offset"].as<int>();
	if(vm.count("chroma_excursion")) format->chroma_excursion=vm["chroma_excursion"].as<int>();
}

void addVideoMetadataOptions(po::options_description &vm_opts)
{
	vm_opts.add_options()
	("interlaced",        fixes::po::value<bool>(),                 "bool    Source video is interlaced")
	("topfieldfirst",     fixes::po::value<bool>(),                 "bool    Interlace is top field first")
	("clean_width",       fixes::po::value<unsigned int>(),         "uint    Clean area width")
	("clean_height",      fixes::po::value<unsigned int>(),         "uint    Clean area height")
	("left_offset",       fixes::po::value<unsigned int>(),         "uint    Clean area left offset")
	("top_offset",        fixes::po::value<unsigned int>(),         "uint    Clean area top offset")
	("colour_primaries",  fixes::po::value<SchroColourPrimaries>(), "enum    Colour primaries [hdtv|sdtv_525|sdtv_625|cinema]")
	("colour_matrix",     fixes::po::value<SchroColourMatrix>(),    "enum    Colour Matrix [hdtv|sdtv|reversible]")
	("transfer_function", fixes::po::value<SchroTransferFunction>(),"enum    Transfer Function [tv_gamma|ext_gamma|linear|dci_gamma]");
}

void applyVideoMetadataOptions(SchroVideoFormat *format, po::variables_map &vm)
{
	if(vm.count("topfieldfirst")) format->top_field_first=vm["topfieldfirst"].as<bool>();
	if(vm.count("clean_width")) format->clean_width=vm["clean_width"].as<int>();
	if(vm.count("clean_height")) format->clean_height=vm["clean_height"].as<int>();
	if(vm.count("left_offset")) format->left_offset=vm["left_offset"].as<int>();
	if(vm.count("top_offset")) format->top_offset=vm["top_offset"].as<int>();

	if(vm.count("colour_primaries")) format->colour_primaries=vm["colour_primaries"].as<SchroColourPrimaries>();
	if(vm.count("colour_matrix")) format->colour_matrix=vm["colour_matrix"].as<SchroColourMatrix>();
	if(vm.count("transfer_function")) format->transfer_function=vm["transfer_function"].as<SchroTransferFunction>();	
}

void addDynamicOptions(po::options_description &dyn_opts)
{
	int n = schro_encoder_get_n_settings();

	for(int i=0; i<n; i++) {
		const SchroEncoderSetting *s = schro_encoder_get_setting_info(i);
		boost::shared_ptr<po::option_description> od;
				std::stringstream text;

		switch(s->type) {
		case SCHRO_ENCODER_SETTING_TYPE_BOOLEAN:
			text << "bool";
			od.reset(new po::option_description(s->name, fixes::po::value<bool>(), text.str().c_str()));
			break;

		case SCHRO_ENCODER_SETTING_TYPE_INT:
			text << "int     min=" << (int)s->min << " max=" << (int)s->max;
			od.reset(new po::option_description(s->name, fixes::po::value<int>(), text.str().c_str()));
			break;

		case SCHRO_ENCODER_SETTING_TYPE_ENUM:
			text << "enum    min=" << s->min << " max=" << s->max << "  [";
			for(int j=0; j<=s->max; j++) {
				text << s->enum_list[j];
				if(j!=s->max) text << "|";
			}
			text << "]";
			od.reset(new po::option_description(s->name, fixes::po::value<std::string>(), text.str().c_str()));
			break;

		case SCHRO_ENCODER_SETTING_TYPE_DOUBLE:
			text << "double  min=" << s->min << " max=" << s->max;
			od.reset(new po::option_description(s->name, fixes::po::value<double>(), text.str().c_str()));
			break;

		default:
			continue;
		}

		dyn_opts.add(od);
	}

}

bool applyDynamicOptions(SchroEncoder *encoder, po::variables_map &vm)
{
	bool ret=false;

#define SET(name, type) \
	if(vm.count(name)) schro_encoder_setting_set_double(encoder, name, vm[name].as<type>())

#define SET_MINMAX(name, type, min, max) \
	if(vm.count(name)) { \
		type t = vm[name].as<type>(); \
		if(t >= min && t <= max) \
			schro_encoder_setting_set_double(encoder, name, t); \
		else { \
			std::stringstream err; \
			err << "Value '" << t << "' for parameter " << name; \
			err << "is not within allowed range " << min << " to " << max << std::endl; \
			throw std::runtime_error(err.str()); \
		} \
	}

#define SET_ENUM(name) \
	if(vm.count(name)) { \
		int index=-1; \
		char *end; \
		std::string str(vm[name].as<std::string>()); \
		for(int j=0; j<=s->max; j++) { \
			if(strcmp(s->enum_list[j], str.c_str()) == 0) \
				index = j; \
			if((strtol(str.c_str(), &end, 10) == (long)j) && (*end == '\0')) \
				index = j; \
		} \
		if(index<0) { \
			std::stringstream err; \
			err << "Value '" << str << "' for parameter " << name; \
			err << " is not an allowed value" << std::endl; \
			throw std::runtime_error(err.str()); \
		} \
		else \
			schro_encoder_setting_set_double(encoder, name, (double)index); \
	}

	try {
		int n = schro_encoder_get_n_settings();
		for(int i=0; i<n; i++)
		{
			const SchroEncoderSetting *s= schro_encoder_get_setting_info(i);

			switch(s->type)
			{
			case SCHRO_ENCODER_SETTING_TYPE_BOOLEAN:
				SET(s->name, bool);
				break;

			case SCHRO_ENCODER_SETTING_TYPE_INT:
				SET_MINMAX(s->name, int, (int)s->min, (int)s->max);
			break;

			case SCHRO_ENCODER_SETTING_TYPE_ENUM:
				SET_ENUM(s->name);
			break;

			case SCHRO_ENCODER_SETTING_TYPE_DOUBLE:
				SET_MINMAX(s->name, double, s->min, s->max);
				break;

			default:
				break;
			}
		}
	}
	catch(std::exception& e)
	{
		std::cerr << "Command line error : " << e.what() << "\n";
		ret = true;
	}

	return ret;
}	
