/* Schrodinger encoder wrapper for non GStreamer applications
 * 2006 Jon Rosser BBC R&D
 *
 */

#undef _FORTIFY_SOURCE
#define _FORTIFY_SOURCE 0

#include <iostream>
#include <stdio.h>
#include <limits.h>

#include <schroedinger/schro.h>

#include "schrooptions.h"
#include <boost/program_options.hpp>
#include "df_boost_prog_opts.hpp"
namespace po = boost::program_options;

#include "common.h"

/* global variables */
static std::string inFileName;
static std::string outFileName;
static FILE *inFile;
static FILE *outFile;
static int start_pos = 0;
static int end_pos = -1;
static SchroVideoFormat *format = NULL;
static SchroEncoder *encoder = NULL;
static bool verbose = false;


/* function prototypes */
static bool parseCommandLine(int argc, char *argv[]);

void
frame_free (SchroFrame* frame, void* priv)
{
  free (priv);
}

/* main coding loop */
int main(int argc, char *argv[])
{
	SchroBuffer *buffer = NULL;
	SchroFrame *frame = NULL;
	int presentation_frame;
	int inFrame = 0;
	int go = 1;

	schro_init();
	encoder = schro_encoder_new();
	format = schro_encoder_get_video_format(encoder);

	if(parseCommandLine(argc, argv)) {
		exit(1);
	}

	if(verbose) {
		schro_debug_set_level (SCHRO_LEVEL_DEBUG);
		showVideoFormat(format);
		showEncoderSettings(encoder);
	}

	if(schro_video_format_validate(format) != 1) {
		fprintf(stderr,"Video format is not valid for encoder\n");
		exit(1);
	}

	schro_encoder_set_video_format(encoder, format);
	size_t frame_size = (size_t)(format->width*format->height);

	switch (format->chroma_format)
	{
	case SCHRO_CHROMA_444:
		frame_size *= 3;
		break;
	case SCHRO_CHROMA_422:
		frame_size *= 2;
		break;
	case SCHRO_CHROMA_420:
	default:
		frame_size = (frame_size*3)/2;
		break;
	}

	/* open an input and output files */
	if (strcmp(inFileName.c_str(), "-") == 0)
	{
		inFileName = std::string("/dev/stdin");
	}
	inFile = fopen(inFileName.c_str(), "rb");
	if (inFile == NULL)
	{
		fprintf(stderr, "Could not open input file %s\n", inFileName.c_str());
		exit(1);
	}

	if (strcmp(outFileName.c_str(), "-") == 0)
	{
		outFileName = std::string("/dev/stdout");
	}
	outFile = fopen(outFileName.c_str(), "wb");
	if (outFile == NULL)
	{
		fprintf(stderr, "Could not open output file %s\n", outFileName.c_str());
		exit(1);
	}

	/* seek into the file if required to satisfy start_pos */
	if (start_pos != 0) fseek(inFile, (frame_size * start_pos), SEEK_CUR);

	/* if end_pos is -1, make lots of frames */
	if (end_pos == -1)
		end_pos = INT_MAX;

	/* encode the required number of frames */
	schro_encoder_start (encoder);
	do {
		switch(schro_encoder_wait(encoder))
		{
		case SCHRO_STATE_NEED_FRAME:
		{

			unsigned char *buf = (unsigned char *)malloc(frame_size);
			/* read from input file into frame */
			if (inFrame > (end_pos - start_pos) ||fread (buf, 1, frame_size, inFile) != frame_size)
			{
				free(buf);
				schro_encoder_end_of_stream(encoder);
			}
			else
			{
				/* make a new frame */
				switch (format->chroma_format)
				{
				case SCHRO_CHROMA_444:
					frame = frame_new_from_data_u8_planar444(buf, format->width, format->height);
					break;
				case SCHRO_CHROMA_422:
					frame = frame_new_from_data_u8_planar422(buf, format->width, format->height);
					break;
				case SCHRO_CHROMA_420:
					frame = schro_frame_new_from_data_I420(buf, format->width, format->height);
					break;
				default:
					fprintf(stderr, "error: unknown chroma format\n");
					exit(1);
					break;
				}
				if(verbose) {
					fprintf(stderr, "Pushing frame %d \r", inFrame++);
					fflush(stderr);
				}
                                schro_frame_set_free_callback (frame, frame_free, buf);
				schro_encoder_push_frame(encoder, frame);
			}
			break;
		}
		case SCHRO_STATE_HAVE_BUFFER:
			buffer = schro_encoder_pull (encoder, &presentation_frame);
			fwrite(buffer->data, 1, buffer->length, outFile);
			schro_buffer_unref (buffer);
			break;

		case SCHRO_STATE_AGAIN:
			break;

		case SCHRO_STATE_END_OF_STREAM:
			// pull end of stream info
			buffer = schro_encoder_pull (encoder, &presentation_frame);
			fwrite(buffer->data, 1, buffer->length, outFile);
			schro_buffer_unref (buffer);
			go = 0;
			break;

		default:
			break;
		}
	}
	//while ((inFrame <= (end_pos - start_pos)) && (feof(inFile) == 0) && go == 1);
	while (go == 1);

	fprintf(stderr, "\nFinished encoding\n");
	schro_encoder_free(encoder);

	fclose(inFile);
	fclose(outFile);
	return 0;
}

bool parseCommandLine(int argc, char **argv)
{
	int wrap_length=800;	//make the internal line from boost have a really long line. The terminal does wrapping instead
	po::variables_map vm;

	try {

		//Video format options
		po::options_description video_coding_options("Video Format Options Used During Coding", wrap_length);
		addVideoCodingOptions(video_coding_options);

		po::options_description video_metadata_options("Video Format MetaData", wrap_length);
		addVideoMetadataOptions(video_metadata_options);

		po::options_description other_options("Other", wrap_length);
		other_options.add_options()
		("start",             fixes::po::value(&start_pos),      "uint    0     \t  Frame number to start coding")
		("stop",              fixes::po::value(&end_pos),        "uint    EOF   \t  Frame number to stop coding")
		("verbose,v",         fixes::po::bool_switch(&verbose),  "Show encoder settings before encoding")
		("input",             fixes::po::value(&inFileName),     "Planar YUV file to encode")
		("output",            fixes::po::value(&outFileName),    "Dirac bitstream output file")
		("help",                                          "Show help");

		//add dynamic options
		po::options_description dynamic_options("Encoder Coding Options", wrap_length);
		addDynamicOptions(dynamic_options);

		//merge the groups of option descriptions
		po::options_description all("Allowed Options");
		all.add(video_coding_options).add(video_metadata_options).add(dynamic_options).add(other_options);

		//the filenames are 'positional options' 
		po::positional_options_description p;
		p.add("input", 1);
		p.add("output", 2);

		//do the command line parsing
		po::store(po::command_line_parser(argc, argv). options(all).positional(p).run(), vm);
		po::notify(vm);

		//decode some overall flags
		if(vm.count("help")) {
			std::cerr << "Usage : schro_encode [flags] input output" << std::endl;
			std::cerr << video_coding_options << std::endl;
			std::cerr << video_metadata_options << std::endl;
			std::cerr << dynamic_options << std::endl;
			std::cerr << other_options << std::endl;
			return true;
		}
	}
	catch(std::exception& e)
	{
		fprintf(stderr, "Command line error : ");
		std::cerr << e.what() << "\n";
		return true;
	}

	applyVideoCodingOptions(format, vm);
	applyVideoMetadataOptions(format, vm);
	return applyDynamicOptions(encoder, vm);
}
