
#ifndef __DF_BOOST_PROG_OPTS_HPP
#define __DF_BOOST_PROG_OPTS_HPP

#include <boost/program_options.hpp>

namespace fixes {
namespace po {
	namespace po = boost::program_options;
	/*
	 * handle specifying the same argument multiple times
	 */
	template<class T, class charT = char>
	class typed_value : public po::typed_value<T, charT>
	{
	public:
		typed_value(T* store_to)
		: po::typed_value<T, charT>(store_to)
		{}

		void xparse(boost::any& value_store,
		            const std::vector< std::basic_string<charT> >& new_tokens)
			const {
			/* Handle multiple overriding values by creating a temp
			 * value store and copying the value out at the end */
			boost::any value_store_tmp;
			po::typed_value<T,charT>::xparse(value_store_tmp, new_tokens);
			value_store = value_store_tmp;
		}
	};

	template<class T>
	typed_value<T>*
	value(T* v)
	{
		typed_value<T>* r = new typed_value<T>(v);
		return r;
	}

	template<class T>
	typed_value<T>*
	value()
	{
		// Explicit qualification is vc6 workaround.
		return value<T>(0);
	}

	typed_value<bool>* bool_switch(bool* v, bool default_v=true);
	typed_value<bool>* bool_switch(bool default_v=true);

}; /* po */
}; /* fixes */

#endif
